# Mysteries of the Moon

A mysterious "moonbox" IPTV was recently found in the electronics recycling bin. This project aims to document it, and hopefully flash a proper GNU/Linux distro onto it.


## Hardware

/proc/cpuinfo returns the following

Processor       : ARMv7 Processor rev 0 (v7l)
processor       : 0
BogoMIPS        : 670.23

processor       : 1
BogoMIPS        : 670.23

Features        : swp half thumb fastmult vfp edsp neon vfpv3
CPU implementer : 0x41
CPU architecture: 7
CPU variant     : 0x3
CPU part        : 0xc09
CPU revision    : 0

Hardware        : Amlogic Meson6 g02 customer platform
Revision        : 0020
Serial          : 000000000000000c

- this device has a single core 32 bit arm processor from Amlogic, a popular tv set top SOC vendor. 

/proc/meminfo returns the following

MemTotal:         789452 kB
MemFree:           27416 kB
Buffers:           12104 kB
Cached:           421548 kB
SwapCached:            0 kB
Active:           359000 kB
Inactive:         290248 kB
Active(anon):     216036 kB
Inactive(anon):      248 kB
Active(file):     142964 kB
Inactive(file):   290000 kB
Unevictable:         408 kB
Mlocked:               0 kB
HighTotal:        524288 kB
HighFree:           6304 kB
LowTotal:         265164 kB
LowFree:           21112 kB
SwapTotal:             0 kB
SwapFree:              0 kB
Dirty:                 0 kB
Writeback:             0 kB
AnonPages:        216012 kB
Mapped:            72224 kB
Shmem:               280 kB
Slab:              20556 kB
SReclaimable:      10284 kB
SUnreclaim:        10272 kB
KernelStack:        5776 kB
PageTables:         9184 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:      394724 kB
Committed_AS:    7573156 kB
VmallocTotal:     262144 kB
VmallocUsed:       59396 kB
VmallocChunk:     139204 kB

However, this seems unlikely, as there are 4 NANYA 256mb ram chips. Indeed, there's probably a quarter of it as a ramdisk, that is mounted on /

## Software

A number of strange APKs were found on the device as well.
The android version is 4.2.2, with a corresponding kernel of 3.0.50
A bootleg copy of busybox 1.21.0 is present. 
